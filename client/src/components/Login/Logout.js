import { GoogleLogout } from 'react-google-login';
const clientId = '670837613159-8egoc5jmthi9v0dc5eavo6jdnsjat6v8.apps.googleusercontent.com';

function Logout() {
  const onSuccess = () => {
    console.log("Logout Success");
    localStorage.setItem("email", "");
    localStorage.setItem("name", "");
    localStorage.setItem("googleId", "");
    window.location.reload(false);
  }

  return (
    <div id='signInButton'>
      <GoogleLogout
        clientId={clientId}
        buttonText='Logout'
        onLogoutSuccess={onSuccess}
      />
    </div>
  )
}

export default Logout;